<?php
include_once '../model/PhoneBook.php';
$obj = new PhoneBook();
$obj->delete(@$_GET['id']);

//$showAllData = $obj->trustShow();
$showTustData = $obj->showTrust();
$obj->restor(@$_GET['idr']);

?>

<fieldset>
    <legend>All Deleted Information in Trust</legend>
    <table border="1">
        <tr>
            <td>SL</td>
            <td>First Name</td>
            <td>Last Name</td>
            <td>Phone Number</td>
            <td>Date</td>
            <td>Action</td>
        </tr>
<?php
    $i = 1;
foreach($showTustData as $value){

?>

        <tr>
            <td> <?php echo $i++;?> </td>
            <td><?php echo $value['f_name'];?></td>
            <td><?php echo $value['l_name'];?></td>
            <td><?php echo $value['mobile_number'];?></td>
            <td><?php echo $value['delete_date'];?></td>
            <td><a href="delete.php?idr=<?php echo $value['id'];?>">Restore</a></td>
        </tr>
        <?php $i++;}?>
    </table>
</fieldset>
