<?php
require_once '../model/PhoneBook.php';

$obj = new PhoneBook();
$all = $obj->showOne($_GET['id']);
?>
<fieldset>
    <legend>Information</legend>
    <table border="1">
        <tr>
            <td>First Name</td>
            <td><?php echo $all->f_name; ?></td>
        </tr>
        <tr>
            <td>Last Name</td>
            <td><?php echo $all->l_name; ?></td>
        </tr>
        <tr>
            <td>Phone Number</td>
            <td><?php echo $all->mobile_number; ?></td>
        </tr>
        <tr>
            <td>Action</td>
            <td><a href="index.php">Back</td>
        </tr>
    </table>

</fieldset>

